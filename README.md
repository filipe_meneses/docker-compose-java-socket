# Java socket chat

## Começando

Para usar esse chat, é necessário instalar o [Docker CE](https://docs.docker.com/install).

Para executar as aplicações necessárias para usar, use o comando:

```sh
docker-compose up -d
```

O comando irá montar e executar os containers, sendo eles:

- `server`: Contém o servidor do socket, tem o papel de reenviar as mensagens;
- `client`: Container para iniciar conexões com o servidor.

## Usando

Para usar, execute o script `cliente.sh`, ou o comando abaixo:

```sh
docker-compose exec client /bin/bash -c "java MainCliente"
```

## Como funciona

Duas classes participam da comunicação a `ServidorChat` e a `ClienteChat`, enquanto outras duas classes instanciam essas, sendo elas: `MainServidor` e `MainCliente`.

O `ServidorChat` inicia um `ServerSocket` e para cada conexão cria uma `Thread`, todas as mensagens recebidas por um socket (uma conexão), ele replica a mensagem para todos os outros clientes conectados.

O `ClienteChat` tem o papel de conectar no servidor oferecendo métodos para enviar e receber mensagem.
