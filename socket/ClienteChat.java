import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ClienteChat extends Thread {
	private Socket s;
	private PrintStream pw;
	private Scanner in;
	private String ip = "127.0.0.1";
	private Integer port = 1234;

	public ClienteChat (String ip) {
		this.ip = ip;
	}

	public ClienteChat (String ip, Integer port) {
		this.ip = ip;
		this.port = port;
	}

	public void run () {
		System.out.println("Cliente: Conectando com o servidor");
		try {
			s = new Socket(ip, port);
			pw = new PrintStream(s.getOutputStream());
			in = new Scanner(s.getInputStream());
			while (in.hasNextLine()) {
				onRecebeMensagem(in.nextLine());
			}
			in.close();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	public void enviar(String username, String string) throws Exception {
		if (pw != null) {
			String m = username  + " disse: " + string;
			byte[] sourceBytes = m.getBytes(StandardCharsets.UTF_8);
			String data = new String(sourceBytes , StandardCharsets.UTF_8);
			pw.println(m);
		} else {
			System.out.println("Espere um momento, conectando...");
		}
	}

	public void onRecebeMensagem(String mensagem) {
		System.out.println(mensagem);
	}
}
