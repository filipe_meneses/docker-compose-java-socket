import java.util.Scanner;

public class MainCliente {
	public static void main(String[] args) {
		ClienteChat s;
		Scanner scanner = new Scanner(System.in);
		String message = "";
		String username = "";

		System.out.println("Para sair, digite \"sair\"\nInsira seu username:");

		username = scanner.nextLine();

		System.out.println("Bem vindo, " + username);

		try {
			s = new ClienteChat("server");
			s.start();
			while (!message.equals("sair")) {
				if (scanner.hasNextLine()) {
					message = scanner.nextLine();
				} else {
					message = null;
				}

				System.out.print(String.format("\033[%dA",1));
				System.out.print("\033[2K");

				if (message != null) {
					s.enviar(username, message);
				}
			}
			System.out.println("Adeus");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
