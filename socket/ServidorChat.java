import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class ServidorChat {
	private ArrayList<PrintWriter> clientes = new ArrayList<PrintWriter>();
	private ServerSocket sv;

	public ServidorChat() throws Exception {
		System.out.println("Servidor: Iniciando chat");
		sv = new ServerSocket(1234);
	}

	public void iniciar() {
	    try {
			iniciar(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void iniciar(boolean broadcast) throws Exception {
		Socket s;
		while ((s = sv.accept()) != null) {
			new Thread(new ServidorChatOuveCliente(s)).start();
			clientes.add(new PrintWriter(s.getOutputStream(), true));
		}
		s.close();
	}

	private class ServidorChatOuveCliente implements Runnable {
		Scanner s;

		public ServidorChatOuveCliente(Socket socket) throws IOException {
			s = new Scanner(socket.getInputStream());
			System.out.println("Novo cliente");
		}

		@Override
		public void run() {
			try {
				while (true) {
					String str = "Erro";
					if (s.hasNextLine()) {
						str = s.nextLine();
						broadcast(str);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void broadcast(String str) {
			System.out.println("Broadcast:" + str);
			for (PrintWriter pw : clientes) {
				pw.println(str);
			}
		}
	}
}
